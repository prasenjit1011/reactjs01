<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


include_once './config/database.php';
require "../vendor/autoload.php";
use \Firebase\JWT\JWT;

$secret_key = "YOUR_SECRET_KEY";
$data = json_decode(file_get_contents("php://input"));
$header = apache_request_headers(); 
$jwt = $header['Authorization'];

if($jwt){
    try {
        $decoded = JWT::decode($jwt, $secret_key, array('HS256'));
		//print_r($decoded);
        echo json_encode(array(
            "message" => "Access granted: ".$jwt
        ));
 
    }catch (Exception $e){
 
    http_response_code(401);
 
    echo json_encode(array(
        "messagess" => "Access denied."
    ));
}
 
}
?>