import React, { Component }  from 'react';
import First   from './First';
import Second   from './Second';
import CustomLinkExample   from './CustomLinkExample';
import FileUploadForm  from "./FileUploadForm"
import logo from './logo.svg';
import './App.css';
 
class App extends Component{  
	render(){  
		return(  
			<div style={{float:'left',marginLeft:'5px',backgroundColor:'#71a068'}}>
				<div style={{float:'left'}}>
					<img src={logo} className="App-logo" alt="logo" width="100" />
				</div>
				<div style={{float:'left',width:'300px'}}>
					<CustomLinkExample />
					<First />
					<FileUploadForm />
				</div>	
				<div style={{float:'left',width:'600px',maxWidth:'600px'}}>
					<Second />
					<hr />
				</div>
			</div>  
		);  
	}  
}  
export default App;
