import React from 'react'
import axios from 'axios';

class FileUploadForm extends React.Component {
    UPLOAD_ENDPOINT = 'http://127.0.0.1/projects/reactjs/upload.php';
	constructor(props) {
        super(props);
        this.state ={
          file:null
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.uploadFile = this.uploadFile.bind(this)		
    }
    async uploadFile(file){
		const formData = new FormData();
		formData.append('avatar',file)
		return  await axios.post(this.UPLOAD_ENDPOINT, formData,{
			headers: {
				'content-type': 'multipart/form-data'
			}
		});
	}

    async onSubmit(e){
        e.preventDefault() 
        let res = await this.uploadFile(this.state.file);
        console.log(res.data);
    }
    onChange(e) {
        this.setState({file:e.target.files[0]})
    }

	
	render() {
        return (
			<div style={{border:'1px solid #FFF', marginTop:'10px', padding:'10px'}}>
				<form onSubmit={ this.onSubmit }>
					<h5> React File Upload</h5>
					<input type="file" style={{width:'200px'}} onChange={ this.onChange } />
					<button type="submit">Upload</button>
				</form>
			</div>
       )
      }

	
}
export default FileUploadForm;
